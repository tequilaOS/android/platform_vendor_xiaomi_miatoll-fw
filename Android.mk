#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

LOCAL_PATH := $(call my-dir)

ifneq ($(filter miatoll,$(TARGET_DEVICE)),)
$(call add-radio-file,BTFM.bin)
$(call add-radio-file,dspso.bin)
$(call add-radio-file,NON-HLOS.bin)
endif
